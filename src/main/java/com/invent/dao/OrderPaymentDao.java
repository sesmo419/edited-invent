package com.invent.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invent.models.OrderPaymentModel;
@Repository
public interface OrderPaymentDao extends JpaRepository<OrderPaymentModel, Long> {
	public List<OrderPaymentModel> findOrderByOrderId(Long orderId);

}
