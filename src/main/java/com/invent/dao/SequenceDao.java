package com.invent.dao;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invent.models.SequenceModel;

@Repository
public interface SequenceDao extends JpaRepository<SequenceModel, Long> {
	
	@Transactional
	@Modifying(clearAutomatically=true)
	@Query("UPDATE SequenceModel sequences SET sequences.orderNo=sequences.orderNo+:orderNo WHERE sequences.id=:id")
	public int generateOrderNo(@Param("id")Long id,@Param("orderNo")Long orderNo);

}
