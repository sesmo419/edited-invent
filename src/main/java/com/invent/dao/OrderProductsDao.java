package com.invent.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invent.models.OrderProductsModel;
@Repository
public interface OrderProductsDao extends JpaRepository<OrderProductsModel, Long> {

	public List<OrderProductsModel> findOrderProductsByOrderId(long orderId);
	public List<OrderProductsModel> findProductsByProductNo(long productNo);
	public List<OrderProductsModel> findOrderProductsByProductNo(String productNo);

}
