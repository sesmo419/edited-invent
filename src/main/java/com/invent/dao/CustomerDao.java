package com.invent.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.invent.models.CustomerModel;

@Repository
public interface CustomerDao extends JpaRepository<CustomerModel, Long> {
	
	@Query(value="SELECT * FROM customers WHERE customers.customer_name>=?1 AND customers.customer_name<=?1",nativeQuery=true)
	public List<CustomerModel> findCustomerByCustomerName(@Param("name") String name);

}
