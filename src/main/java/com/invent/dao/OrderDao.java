package com.invent.dao;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.invent.models.OrderModel;
@Repository
public interface OrderDao extends JpaRepository<OrderModel, Long> {
	@Query(value="SELECT * FROM orders WHERE orders.order_date>=?1", nativeQuery=true)
	public List<OrderModel> findOrderByOrderDate(@Param ("date") Date order_date);
//	@Query("UPDATE ItemsModel items SET items.quantity=(items.quantity-(SELECT orders.quantity FROM OrderModel orders WHERE orders.SALEID=")
//	public void makeOrder(OrderModel order);

}
