package com.invent.controllers;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invent.dao.CustomerDao;
import com.invent.dao.OrderDao;
import com.invent.exception.NotFoundException;
import com.invent.models.CustomerModel;
import com.invent.models.OrderModel;
import com.invent.service.CustomerService;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private CustomerDao customerDao;
	@GetMapping("/")
	public List<CustomerModel> getAllCustomersRecords() {
		
		return customerService.getAllCustomersRecords();
	}
	@GetMapping("/{id}")
	public Optional<CustomerModel> getCustomerRecordById(@PathVariable Long id) {
		
		return customerService.getCustomerRecordById(id);
	}
	@GetMapping("/name")
	public List<CustomerModel> getCustomerRecordByCustomerName(@RequestParam("name") String customerName) {
		
		return customerService.getCustomerRecordByCustomerName(customerName);
	}

	@PostMapping("/save")
	public void addCustomerRecord(@RequestBody CustomerModel customer) {
	
		customerService.addCustomerRecord(customer);;
	}
	@PostMapping("/{orderId}")
	public CustomerModel addCustomerToOrder(@RequestBody CustomerModel customer, @PathVariable Long orderId) {
		
		return orderDao.findById(orderId).map(order->{
			customer.setOrders(order);
			return customerDao.save(customer);
		}).orElseThrow(() -> new NotFoundException("Order not found!"));
	}


	@DeleteMapping("/{id}")
	public void deleteCustomerRecordById(@PathVariable Long id) {
		
		customerService.deleteCustomerRecordById(id);;
	}

	@PutMapping("/{id}")
	public void updateCustomerRecord(@RequestBody CustomerModel customer, @PathVariable Long id) {
		
		customerService.updateCustomerRecord(customer,id);
	}

}
