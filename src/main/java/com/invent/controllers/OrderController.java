package com.invent.controllers;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invent.dao.CustomerDao;
import com.invent.dao.OrderDao;
import com.invent.exception.NotFoundException;
import com.invent.models.OrderModel;
import com.invent.service.OrderService;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private CustomerDao customerDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@GetMapping("/")
	public List<OrderModel> getAllOrders() {
		
		return orderService.getAllOrders();
	}

	@GetMapping("/date")
	public List<OrderModel> getAllOrdersByDate( @RequestParam("date")  @DateTimeFormat(pattern="dd-MM-yyyy") Date orderDate) {
		
		return orderService.getAllOrdersByDate(orderDate);
	}
	
	@PostMapping("/{customerId}")
	public OrderModel makeOrder(@RequestBody OrderModel order, @PathVariable Long customerId) {
		
		return customerDao.findById(customerId).map(customer->{
			order.setCustomer(customer);
			return orderDao.save(order);
		}).orElseThrow(() -> new NotFoundException("Vendor not found!"));
		
		
	}

	@GetMapping("/{id}")
	public OrderModel getOrderById(@PathVariable("id") Long id) {
		
		return orderService.getOrderById(id);
  }
  
  @PostMapping("/")
  public OrderModel newOrder() {
    return null;
  }


}
