package com.invent.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.invent.dao.OrderDao;
import com.invent.dao.OrderProductsDao;
import com.invent.dao.ProductsDao;
import com.invent.exception.NotFoundException;
import com.invent.models.OrderProductsModel;
import com.invent.models.ProductsModel;
import com.invent.service.OrderProductsService;

@RestController
@RequestMapping("/api/order-products")
public class OrderProductsController {
	
	@Autowired
	private OrderProductsService orderProductsService;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private OrderProductsDao orderProductsDao;
	@Autowired
	private ProductsDao productDao;
	
	@GetMapping("/{orderId}")
	public List<OrderProductsModel> getAllOrderProductsByOrderId(@PathVariable Long orderId) {
		
		return orderProductsService.getAllOrderProductsByOrderId(orderId);
	}

	@DeleteMapping("/{id}")
	public void deleteOrderProductById(@PathVariable Long id) {
		
		orderProductsService.deleteOrderProductById(id);;
	}
	
	@PostMapping("/{orderId}/{id}/quantity")
	public OrderProductsModel addOrderProduct(@RequestBody OrderProductsModel orderProducts, @PathVariable Long orderId,@PathVariable Long id,@Param("quantity")int quantity) { 
		productDao.decreaseProductQuantity(id, quantity);
		return orderDao.findById(orderId).map(order->{
			orderProducts.setOrder(order);
			return orderProductsDao.save(orderProducts);
		}).orElseThrow(() -> new NotFoundException("Order not found!"));
	}

		
		
	
	
}
