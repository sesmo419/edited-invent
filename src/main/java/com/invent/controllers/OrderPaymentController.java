package com.invent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.invent.dao.OrderDao;
import com.invent.dao.OrderPaymentDao;
import com.invent.exception.NotFoundException;
import com.invent.models.OrderPaymentModel;
import com.invent.service.OrderPaymentService;

@RestController
@RequestMapping("/api/order-payments")
public class OrderPaymentController {
	
	@Autowired
	private OrderPaymentService orderPaymentService;
	
	@Autowired
	private OrderPaymentDao orderPaymentDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@GetMapping("/")
	public List<OrderPaymentModel> getAllPaymentMethods() {
		
		return orderPaymentService.getAllPaymentMethods();
	}
	
	@PostMapping("/{orderId}")
	public OrderPaymentModel makeOrderPayment(@RequestBody OrderPaymentModel orderPayment, @PathVariable Long orderId) {
		
		return orderDao.findById(orderId).map(order->{
			orderPayment.setOrder(order);
			return orderPaymentDao.save(orderPayment);
		}).orElseThrow(() -> new NotFoundException("Order not found!"));
	}


}
