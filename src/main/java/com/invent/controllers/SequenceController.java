package com.invent.controllers;

import java.sql.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.invent.models.SequenceModel;
import com.invent.service.SequenceService;

@RestController
@RequestMapping("/api/sequence")
public class SequenceController {
	
	@Autowired
	private SequenceService sequenceService;
	
	
	@GetMapping("/{id}")
	public Optional<SequenceModel> getOrderNoById(@PathVariable Long id) {
		
		return sequenceService.getOrderNoById(id);
	}
	
//	@PutMapping("/{id}")
//	public void generateOrderNo(@RequestBody SequenceModel sequence,@PathVariable Long id) {
//		sequenceService.generateOrderNo(sequence, id);
//		
//	}
	@PutMapping("/{id}/order")
	public int generateOrderNo(@PathVariable Long id,@Param("orderNo") Long orderNo) {
		
		
		return sequenceService.generateOrderNo(id, orderNo);
	}

}
