package com.invent.service;

import java.util.List;
import java.util.Optional;

import com.invent.models.CustomerModel;

public interface CustomerService {
	public List<CustomerModel> getAllCustomersRecords();
	public List<CustomerModel> getCustomerRecordByCustomerName(String customerName);
	public Optional<CustomerModel> getCustomerRecordById(Long id);
	public void addCustomerRecord(CustomerModel customer);
	public void deleteCustomerRecordById(Long id);
	public void updateCustomerRecord(CustomerModel customer,Long id);
	public CustomerModel addCustomerToOrder(CustomerModel customer,Long orderId);
}
