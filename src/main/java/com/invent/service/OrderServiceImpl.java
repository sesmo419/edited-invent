package com.invent.service;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invent.dao.OrderDao;
import com.invent.models.OrderModel;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;
	
	@Override
	public List<OrderModel> getAllOrders() {
		
		return orderDao.findAll();
	}

	@Override
	public List<OrderModel> getAllOrdersByDate(Date order_date) {
		
		return orderDao.findOrderByOrderDate(order_date);
	}

	@Override
	public OrderModel getOrderById(Long id) {
		
		return orderDao.getOne(id);
	}



}
