package com.invent.service;

import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.invent.models.SequenceModel;

public interface SequenceService {
	
	//public void generateOrderNo(SequenceModel sequence,Long id);
	public Optional<SequenceModel> getOrderNoById(Long id);
	public int generateOrderNo(Long id,Long orderNo);

}
