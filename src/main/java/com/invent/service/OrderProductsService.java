package com.invent.service;

import java.util.List;

import com.invent.models.OrderProductsModel;

public interface OrderProductsService {
	
	public List<OrderProductsModel> getAllOrderProductsByOrderId(Long orderId);
	public void deleteOrderProductById(Long id);
	public OrderProductsModel addOrderProduct(OrderProductsModel orderProducts, Long orderId);

}
