package com.invent.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invent.dao.SequenceDao;
import com.invent.models.SequenceModel;

@Service
public class SequenceServiceImpl implements SequenceService {
	
	@Autowired
	private SequenceDao sequenceDao;
	
	@Override
	public Optional<SequenceModel> getOrderNoById(Long id) {
	
		return sequenceDao.findById(id);
	}

//	@Override
//	public void generateOrderNo(SequenceModel sequence,Long id) {
//		sequenceDao.save(sequence);
//		
//	}

	@Override
	public int generateOrderNo(Long id, Long orderNo) {
		
		return sequenceDao.generateOrderNo(id, orderNo);
	}



}
