package com.invent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invent.dao.ProductsDao;
import com.invent.dao.OrderDao;
import com.invent.dao.OrderProductsDao;
import com.invent.exception.NotFoundException;
import com.invent.models.ProductsModel;
import com.invent.models.OrderProductsModel;

@Service
public class OrderProductsServiceImpl implements OrderProductsService {

	@Autowired
	private OrderProductsDao orderProductsDao;
	@Autowired
	private ProductsDao productDao;

	@Autowired
	private OrderDao orderDao;
	
	
	@Override
	public List<OrderProductsModel> getAllOrderProductsByOrderId(Long orderId) {
		
		return orderProductsDao.findOrderProductsByOrderId(orderId);
	}

	@Override
	public void deleteOrderProductById(Long id) {
		
		orderProductsDao.deleteById(id);
	}

	@Override
	public OrderProductsModel addOrderProduct(OrderProductsModel orderProducts, Long orderId) {
		
		return orderProductsDao.save(orderProducts);
	}

//	@Override
//	public OrderProductsModel addOrderProduct(OrderProductsModel orderProducts, Long orderId) {
//		ProductsModel productOnOrder = productDao.findByProductNo(orderProducts.getProductNo());
//		int newQuantity = productOnOrder.getQuantity() - orderProducts.getQuantity();
//		productOnOrder.setQuantity(newQuantity);
//		productDao.save(productOnOrder);
//		return orderDao.findById(orderId).map(order->{
//			orderProducts.setOrder(order);
//			return orderProductsDao.save(orderProducts);
//		}).orElseThrow(() -> new NotFoundException("Order not found!"));		
//	}

	

}
