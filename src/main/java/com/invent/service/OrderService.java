package com.invent.service;

import java.util.Date;
import java.util.List;
import com.invent.models.OrderModel;

public interface OrderService {
	
	public List<OrderModel> getAllOrders();
	public List<OrderModel> getAllOrdersByDate(Date order_date);
	public OrderModel getOrderById(Long id);

}
