package com.invent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invent.dao.OrderPaymentDao;
import com.invent.models.OrderPaymentModel;

@Service
public class OrderPaymentServiceImpl implements OrderPaymentService{

	@Autowired
	private OrderPaymentDao orderPaymentDao;
	
	@Override
	public List<OrderPaymentModel> getAllPaymentMethods() {
		
		return orderPaymentDao.findAll();
	}

	@Override
	public OrderPaymentModel makeOrderPayment(OrderPaymentModel orderPayment, Long orderId) {
		
		return orderPaymentDao.save(orderPayment);
	}

	
	
}
