package com.invent.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invent.dao.CustomerDao;
import com.invent.models.CustomerModel;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerDao customerDao;
	
	@Override
	public List<CustomerModel> getAllCustomersRecords() {
		
		return customerDao.findAll();
	}

	@Override
	public List<CustomerModel> getCustomerRecordByCustomerName(String customerName) {
		
		return customerDao.findCustomerByCustomerName(customerName);
	}

	@Override
	public void addCustomerRecord(CustomerModel customer) {
	
		customerDao.save(customer);
	}

	@Override
	public void deleteCustomerRecordById(Long id) {
		
		customerDao.deleteById(id);
	}

	@Override
	public void updateCustomerRecord(CustomerModel customer,Long id) {
		
		customerDao.save(customer);
	}

	@Override
	public Optional<CustomerModel> getCustomerRecordById(Long id) {
		
		return customerDao.findById(id);
	}

	@Override
	public CustomerModel addCustomerToOrder(CustomerModel customer, Long orderId) {
		
		return customerDao.save(customer);
	}


}
