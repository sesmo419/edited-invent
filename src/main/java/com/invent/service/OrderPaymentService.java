package com.invent.service;

import java.util.List;

import com.invent.models.OrderPaymentModel;

public interface OrderPaymentService {
	public List<OrderPaymentModel> getAllPaymentMethods();
	public OrderPaymentModel makeOrderPayment(OrderPaymentModel orderPayment, Long orderId);

}
