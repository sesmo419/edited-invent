package com.invent.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sequences")
public class SequenceModel {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long orderNo;
	public SequenceModel() {
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(long orderNo) {
		this.orderNo = orderNo;
	}

}
