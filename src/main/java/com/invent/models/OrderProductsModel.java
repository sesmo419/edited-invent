package com.invent.models;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="order_products")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class OrderProductsModel implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="product_name")
	private String productName;
	@Column(name="product_no")
	private String productNo;
	private int quantity;
	@Column(name="unit_price")
	private int unitPrice;
	@ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name = "order_id")
	@JsonIgnore
	private OrderModel order;
	@ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "product_id")
	@JsonIgnore
	private ProductsModel product;
	public OrderProductsModel() {}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductNo() {
		return productNo;
	}
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}
	public OrderModel getOrder() {
		return order;
	}
	public void setOrder(OrderModel order) {
		this.order = order;
	}
	public ProductsModel getProduct() {
		return product;
	}
	public void setProduct(ProductsModel product) {
		this.product = product;
	}



}
