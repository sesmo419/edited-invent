package com.invent.models;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="orders")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class OrderModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_no")
	private long orderNo;
	@Temporal(TemporalType.DATE)
	@Column(name="order_date", columnDefinition="DATETIME")
	private Date orderDate;
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<OrderProductsModel> orderProducts;
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<OrderPaymentModel> orderPayments;
	@ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name = "customer_id")
	@JsonIgnore
	private CustomerModel customer;
	public OrderModel() {}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(long orderNo) {
		this.orderNo = orderNo;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Set<OrderProductsModel> getOrderProducts() {
		return orderProducts;
	}
	public void setOrderProducts(Set<OrderProductsModel> orderProducts) {
		this.orderProducts = orderProducts;
	}

	public Set<OrderPaymentModel> getOrderPayments() {
		return orderPayments;
	}
	public void setOrderPayments(Set<OrderPaymentModel> orderPayments) {
		this.orderPayments = orderPayments;
	}
	public CustomerModel getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}
	
	
	

	
}
